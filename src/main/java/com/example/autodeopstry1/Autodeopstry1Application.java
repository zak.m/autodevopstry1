package com.example.autodeopstry1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Autodeopstry1Application {

	public static void main(String[] args) {
		SpringApplication.run(Autodeopstry1Application.class, args);
	}

}
